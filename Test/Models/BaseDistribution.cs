﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace Test.Models
{
    public class EntityKeyedString
    {
        public string ID { get; set; }
    }


    public class Supplier : BaseOrganization
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrgTypeID { get; set; }

        private ICollection<SupplierCert> supplierCerts;

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<SupplierCert> SupplierCerts
        {
            get { return supplierCerts ?? (supplierCerts = new List<SupplierCert>()); }
            protected set { supplierCerts = value; }
        }


        private ICollection<Factory> factories;

        public virtual ICollection<Factory> Factorys
        {
            get { return factories ?? (factories = new List<Factory>()); }
            protected set { factories = value; }
        }

        public string AuditUserID { get; set; }
        public string AuditUserName { get; set; }
        public DateTime? AuditTime { get; set; }

        public byte? UpdateToDepStatus { get; set; }

        /// <summary>
        /// 修改标志 1可修改,2不可修改
        /// </summary>
        public byte EnableEdit { get; set; } = 2;
    }
    public class SupplierCert : EntityKeyedString
    {

        /// <summary>
        /// 供应商证件类型ID
        /// </summary>
        public string CertTypeID { get; set; }



        /// <summary>
        /// 供应商代码
        /// </summary>
        public string SupplierID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Supplier Supplier { get; set; }


        /// <summary>
        /// 证件号
        /// </summary>
        public string CertNo { get; set; }

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime? CertDateTime { get; set; }

        /// <summary>
        /// 是否长期有效
        /// </summary>
        public bool? IsLongTerm { get; set; }

        /// <summary>
        /// 证件有效期
        /// </summary>
        public DateTime? CertValidatePeriod { get; set; }

        /// <summary>
        /// 发证部门
        /// </summary>
        public string CertDepart { get; set; }

        /// <summary>
        /// 生产范围
        /// </summary>
        public string ProductionRange { get; set; }

        /// <summary>
        /// 证件审核状态：0，未审核；1，审核通过；2，审核未通过
        /// </summary>
        public byte? AuditStatus { get; set; }

        /// <summary>
        /// 证件状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }


        /// <summary>
        /// 
        /// </summary>
        private ICollection<SupplierCertPicture> supplierCertPictures;

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<SupplierCertPicture> SupplierCertPictures
        {
            get { return supplierCertPictures ?? (supplierCertPictures = new List<SupplierCertPicture>()); }
            protected set { supplierCertPictures = value; }
        }

        /// <summary>
        /// 原历史证件ID 
        /// </summary>
        public string SourceCertId { get; set; }
    }
    public class SupplierCertPicture : EntityKeyedString
    {

        /// <summary>
        /// 证件ID 
        /// </summary>
        public string SupplierCertID { get; set; }

        /// <summary> 
        /// 
        /// </summary>
        public virtual SupplierCert SupplierCert { get; set; }


        /// <summary>
        /// 证件图片名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件图片类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 图片存放目录
        /// </summary>
        public string PictureCatalog { get; set; }

        /// <summary>
        /// 证件图片状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }
    }
    public class Factory : BaseOrganization
    {
        public byte? UpdateToBaseStatus { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public string OrgTypeID { get; set; }


        /// <summary>
        /// 
        /// </summary>
        private ICollection<FactoryCert> factoryCerts;

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<FactoryCert> FactoryCerts
        {
            get { return factoryCerts ?? (factoryCerts = new List<FactoryCert>()); }
            //protected set { factoryCerts = value; }
            set { factoryCerts = value; }
        }


        public string SupplierID { get; set; }

        public virtual Supplier Supplier { get; set; }


        public string AuditUserID { get; set; }
        public string AuditUserName { get; set; }
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 上传dep标识
        /// </summary>
        public byte? UpdateToDepStatus { get; set; } = 0;

        /// <summary>
        /// 来源类型 1.手工录入 2.平台导入
        /// </summary>
        public byte SourceType { get; set; } = 1;

        /// <summary>
        /// 生产厂商历史信息  此字段存供应商输入的数据，json形式保存
        /// </summary>
        public string HistoryInfo { get; set; }

        /// <summary>
        /// 修改标志 1可修改,2不可修改
        /// </summary>
        public byte EnableEdit { get; set; } = 2;
    }
    public abstract class BaseOrganization : EntityKeyedString
    {
        /// <summary> 营业执照号码 </summary>
        public string Code { get; set; }

        /// <summary> 名称 </summary>
        public string Name { get; set; }

        /// <summary> 营业执照 </summary>
        public string SocialCreditID { get; set; }

        /// <summary> 法人 </summary>
        public string Conventional { get; set; }

        /// <summary> 法人电话 </summary>
        public string ConventionalTel { get; set; }

        /// <summary> 法人邮箱 </summary>
        public string ConventionalEMail { get; set; }

        /// <summary> 所属地区 </summary>
        public string RegAddrString { get; set; }

        /// <summary> 详细地址 </summary>
        public string DetailAddr { get; set; }

        /// <summary> 拼音简拼 </summary>
        public string PYJM { get; set; }

        /// <summary> 拼音全码 </summary>
        public string PYQM { get; set; }

        /// <summary> 五笔简拼 </summary>
        public string WBJM { get; set; }

        /// <summary> 经营范围 </summary>
        public string ProductionRange { get; set; }

        /// <summary> 联系人 </summary>
        public string Contacts1 { get; set; }

        /// <summary> 联系人电话 </summary>
        public string Tel1 { get; set; }

        /// <summary> 联系人邮箱 </summary>
        public string EMail1 { get; set; }

        /// <summary> 第二联系人 </summary>
        public string Contacts2 { get; set; }

        /// <summary> 第二联系人电话 </summary>
        public string Tel2 { get; set; }

        /// <summary> 第二联系人邮箱 </summary>
        public string EMail2 { get; set; }

        /// <summary> 经营方式 </summary>
        public string ManageType { get; set; }

        /// <summary> 备注 </summary>
        public string Memo { get; set; }

        /// <summary> 审核状态 </summary>
        public byte? AuditStatus { get; set; }

        /// <summary> 禁用状态 </summary>
        public byte Status { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
    public class FactoryCert : EntityKeyedString
    {
        #region 关联字典表中证件类型

        /// <summary>
        /// 证件类型ID
        /// </summary>
        public string CertTypeID { get; set; }

        #endregion

        #region 关联生产厂商ID

        /// <summary>
        /// 生产厂商代码
        /// </summary>
        public string FactoryID { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public virtual Factory Factory { get; set; }

        #endregion

        /// <summary>
        /// 证件号
        /// </summary>
        public string CertNo { get; set; }

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime? CertDateTime { get; set; }

        /// <summary>
        /// 是否长期有效
        /// </summary>
        public bool? IsLongTerm { get; set; }

        /// <summary>
        /// 证件有效期
        /// </summary>
        public DateTime? CertValidatePeriod { get; set; }

        /// <summary>
        /// 发证部门
        /// </summary>
        public string CertDepart { get; set; }

        /// <summary>
        /// 生产范围
        /// </summary>
        public string ProductionRange { get; set; }

        /// <summary>
        /// 证件审核状态：0，未审核；1，审核通过；2，审核未通过
        /// </summary>
        public byte? AuditStatus { get; set; }

        /// <summary>
        /// 证件状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }


        /// <summary>
        /// 原历史证件ID 
        /// </summary>
        public string SourceCertId { get; set; }
    }


    public class SupplierModel : BaseOrganization
    {
        /// <summary>
        /// 
        /// </summary>
        public string OrgTypeID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SupplierCertModel> SupplierCerts { get; set; }



        public virtual List<FactoryModel> Factorys { get; set; }

        public string AuditUserID { get; set; }
        public string AuditUserName { get; set; }
        public DateTime? AuditTime { get; set; }

        public byte? UpdateToDepStatus { get; set; }

        /// <summary>
        /// 修改标志 1可修改,2不可修改
        /// </summary>
        public byte EnableEdit { get; set; } = 2;
    }
    public class SupplierCertModel : EntityKeyedString
    {

        /// <summary>
        /// 供应商证件类型ID
        /// </summary>
        public string CertTypeID { get; set; }



        /// <summary>
        /// 供应商代码
        /// </summary>
        public string SupplierID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual SupplierModel Supplier { get; set; }


        /// <summary>
        /// 证件号
        /// </summary>
        public string CertNo { get; set; }

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime? CertDateTime { get; set; }

        /// <summary>
        /// 是否长期有效
        /// </summary>
        public bool? IsLongTerm { get; set; }

        /// <summary>
        /// 证件有效期
        /// </summary>
        public DateTime? CertValidatePeriod { get; set; }

        /// <summary>
        /// 发证部门
        /// </summary>
        public string CertDepart { get; set; }

        /// <summary>
        /// 生产范围
        /// </summary>
        public string ProductionRange { get; set; }

        /// <summary>
        /// 证件审核状态：0，未审核；1，审核通过；2，审核未通过
        /// </summary>
        public byte? AuditStatus { get; set; }

        /// <summary>
        /// 证件状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }



        /// <summary>
        /// 
        /// </summary>
        public virtual List<SupplierCertPictureModel> SupplierCertPictures { get; set; }

        /// <summary>
        /// 原历史证件ID 
        /// </summary>
        public string SourceCertId { get; set; }
    }
    public class SupplierCertPictureModel : EntityKeyedString
    {

        /// <summary>
        /// 证件ID 
        /// </summary>
        public string SupplierCertID { get; set; }

        /// <summary> 
        /// 
        /// </summary>
        public virtual SupplierCertModel SupplierCert { get; set; }


        /// <summary>
        /// 证件图片名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件图片类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 图片存放目录
        /// </summary>
        public string PictureCatalog { get; set; }

        /// <summary>
        /// 证件图片状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }
    }
    public class FactoryModel : BaseOrganization
    {
        public byte? UpdateToBaseStatus { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public string OrgTypeID { get; set; }



        /// <summary>
        /// 
        /// </summary>
        public virtual List<FactoryCertModel> FactoryCerts { get; set; }

        public string SupplierID { get; set; }

        public virtual SupplierModel Supplier { get; set; }


        public string AuditUserID { get; set; }
        public string AuditUserName { get; set; }
        public DateTime? AuditTime { get; set; }
        /// <summary>
        /// 上传dep标识
        /// </summary>
        public byte? UpdateToDepStatus { get; set; } = 0;

        /// <summary>
        /// 来源类型 1.手工录入 2.平台导入
        /// </summary>
        public byte SourceType { get; set; } = 1;

        /// <summary>
        /// 生产厂商历史信息  此字段存供应商输入的数据，json形式保存
        /// </summary>
        public string HistoryInfo { get; set; }

        /// <summary>
        /// 修改标志 1可修改,2不可修改
        /// </summary>
        public byte EnableEdit { get; set; } = 2;
    }
    public class FactoryCertModel : EntityKeyedString
    {
        #region 关联字典表中证件类型

        /// <summary>
        /// 证件类型ID
        /// </summary>
        public string CertTypeID { get; set; }

        #endregion

        #region 关联生产厂商ID

        /// <summary>
        /// 生产厂商代码
        /// </summary>
        public string FactoryID { get; set; }

        /// <summary>
        /// 
        /// </summary>

        public virtual FactoryModel Factory { get; set; }

        #endregion

        /// <summary>
        /// 证件号
        /// </summary>
        public string CertNo { get; set; }

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime? CertDateTime { get; set; }

        /// <summary>
        /// 是否长期有效
        /// </summary>
        public bool? IsLongTerm { get; set; }

        /// <summary>
        /// 证件有效期
        /// </summary>
        public DateTime? CertValidatePeriod { get; set; }

        /// <summary>
        /// 发证部门
        /// </summary>
        public string CertDepart { get; set; }

        /// <summary>
        /// 生产范围
        /// </summary>
        public string ProductionRange { get; set; }

        /// <summary>
        /// 证件审核状态：0，未审核；1，审核通过；2，审核未通过
        /// </summary>
        public byte? AuditStatus { get; set; }

        /// <summary>
        /// 证件状态
        /// </summary>
        public byte? Status { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 索引字段ID
        /// </summary>
        public int? IndexID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Memo { get; set; }


        /// <summary>
        /// 原历史证件ID 
        /// </summary>
        public string SourceCertId { get; set; }
    }
}