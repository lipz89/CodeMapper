﻿using System;
using System.Collections.Generic;

namespace Test.Models
{
    public interface IEntity
    {

    }
    public interface IModel
    {

    }
    /// <summary>
    /// 基础数据-药品-自定义信息
    /// </summary>
    public class JCSJ_YP_ZDYXX : DrugBasicInfo, IEntity
    {
        /// <summary>
        /// 审核状态；1.暂存 | 待提交；2.已提交 | 待审核；3.审核通过；4：不通过
        /// </summary>
        public bool SHZT { get; set; }

        /// <summary>
        /// 提交审核时间
        /// </summary>
        public DateTime? TJSJ { get; set; }

        /// <summary>
        /// 审阅时间
        /// </summary>
        public DateTime? SYSJ { get; set; }

        /// <summary>
        ///审核结束时间
        /// </summary>
        public DateTime? SHJSSJ { get; set; }

        /// <summary>
        /// 审核反馈结果
        /// </summary>
        public string SHFK { get; set; }

        /// <summary>
        /// 上传dep标志(0：未上传，1：上传成功；2：上传失败)
        /// </summary>
        public byte UpDepStatus { get; set; }

        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UpTime { get; set; }
        /// <summary>
        /// 上传返回信息
        /// </summary>
        public string UpMsg { get; set; }
        /// <summary>
        /// 是否需要上传dep；0：不需要上传（一般是只从dep下载（或者已经上传过的）  且未修改过）；1：需要上传dep（从excel导入，数据由修改）
        /// </summary>
        public byte NeedUpToDep { get; set; }
        /// <summary>
        /// 数据来源（0-Excel导入；1-中心下载）
        /// </summary>
        public byte? SJLY { get; set; }
        /// <summary>
        /// 采购类别【中标药品(0)、廉价药品(1)、紧张药品(2)、低价药品(3)、备案药品(4)】
        /// </summary>
        public short? CGLB { get; set; }
        /// <summary>
        /// His编码
        /// </summary>
        public string HISCode { get; set; }
        /// <summary>
        /// 进零加成比 
        /// </summary>
        public decimal? JLJCB { get; set; }
        /// <summary>
        /// 管理标识（1-线上，2-线下）
        /// </summary>
        public byte? GLBS { get; set; }

        /// <summary>
        /// 根客户ID 
        /// </summary>
        public string Curr_CustomerID { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class DrugBasicInfo
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 药品信息 ID JCSJ_YP_BZXX.ID
        /// </summary>
        public string YPBZXXID { get; set; }
        /// <summary>
        /// 药品基础信息表
        /// </summary>
        public virtual JCSJ_YP_BZXX BaseData { get; set; }
        /// <summary>
        /// 基药类型（基药、非基药、省增补）
        /// </summary>
        public string JYLX { get; set; }

        /// <summary>
        /// 是否GPO；0：否；1：是
        /// </summary>
        public bool? GPO { get; set; }
        /// <summary>
        /// 是否托管；0：否；1：是
        /// </summary>
        public bool? TG { get; set; }
        /// <summary>
        /// 是否定量采购；0：否；1：是
        /// </summary>
        public bool? DLCG { get; set; }
        /// <summary>
        /// 医保代码
        /// </summary>
        public string YBDM { get; set; }

        /// <summary>
        /// 阳光码
        /// </summary>
        public string YGM { get; set; }

        /// <summary>
        /// 库存上限
        /// </summary>
        public int? KCSX { get; set; }

        /// <summary>
        /// 库存下限
        /// </summary>
        public int? KCXX { get; set; }

        /// <summary>
        /// 采购药库ID
        /// </summary>
        public string CGYKID { get; set; }

        /// <summary>
        /// 采购药库名称
        /// </summary>
        public string CGYKMC { get; set; }

        /// <summary>
        /// 箱规数量
        /// </summary>
        public int? XGSL { get; set; }

        /// <summary>
        /// 目录类型；1：日常；0：临采
        /// </summary>
        public byte? MLLX { get; set; }

        /// <summary>
        /// 使用单位Code;记录是哪个医疗机构使用的
        /// </summary>
        public string SYDWCode { get; set; }
        /// <summary>
        ///默认供应商id
        /// </summary>
        public string MRGYSID { get; set; }
        /// <summary>
        /// 默认供应商名称
        /// </summary>
        public string MRGYSMC { get; set; }
        /// <summary>
        /// 第三方编码
        /// </summary>
        public string ID3 { get; set; }

        /// <summary>
        /// 中标价
        /// </summary>
        public decimal? ZBJ { get; set; }
        /// <summary>
        /// 进价
        /// </summary>
        public decimal? JJ { get; set; }
        /// <summary>
        /// 零售价
        /// </summary>
        public decimal? LSJ { get; set; }
        /// <summary>
        /// 是否重点监控；0：否 false；1：是 true
        /// </summary>
        public bool? ZDJK { get; set; }
        /// <summary>
        /// 门诊单位ID；字典（盒、支...）
        /// </summary>
        public string MZDWID { get; set; }
        /// <summary>
        /// 门诊单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string MZDWMC { get; set; }
        /// <summary>
        /// 门诊系数
        /// </summary>
        public decimal? MZXS { get; set; }

        /// <summary>
        /// 住院单位ID；字典（盒、支...）
        /// </summary>
        public string ZYDWID { get; set; }
        /// <summary>
        /// 住院单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string ZYDWMC { get; set; }
        /// <summary>
        /// 住院系数
        /// </summary>
        public decimal? ZYXS { get; set; }

        /// <summary>
        /// 药库单位ID；字典（盒、支...）
        /// </summary>
        public string YKDWID { get; set; }
        /// <summary>
        /// 药库单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string YKDWMC { get; set; }
        /// <summary>
        /// 药库系数
        /// </summary>
        public decimal? YKXS { get; set; }
        /// <summary>
        /// 启用标志；1：已启用，0：已停用
        /// </summary>
        public bool QYBZ { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreaterID { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人ID
        /// </summary>
        public string UpdaterID { get; set; }

        /// <summary>
        /// 创建人Code
        /// </summary>
        public string CreaterCode { get; set; }
        /// <summary>
        /// 修改人Code
        /// </summary>
        public string UpdaterCode { get; set; }
        //public decimal? YJ { get; set; }
        //public decimal? PTJ { get; set; }
        //public bool? SF47 { get; set; }
    }

    /// <summary>
    /// 基础数据-药品-标准信息
    /// </summary>
    public class JCSJ_YP_BZXX : IEntity
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 中心数据-药品信息ID
        /// </summary>
        public string ZXYPID { get; set; }

        /// <summary>
        /// 药品名称
        /// </summary>
        public string YPMC { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string SPMC { get; set; }

        /// <summary>
        /// 药品规格
        /// </summary>
        public string YPGG { get; set; }
        /// <summary>
        /// 包装规格 内容是：[药品规格]*[包装系数][最小单位]/[包装单位]
        /// </summary>
        public string BZGG { get; set; }
        /// <summary>
        /// 包装单位；字典ID
        /// </summary>
        public string BZDWID { get; set; }
        /// <summary>
        /// 包装单位名称；字典对应名称
        /// </summary>
        public string BZDWMC { get; set; }

        /// <summary>
        /// 最小单位ID；字典ID
        /// </summary>
        public string ZXDWID { get; set; }
        /// <summary>
        /// 最小单位名称；字典对应名称
        /// </summary>
        public string ZXDWMC { get; set; }
        /// <summary>
        /// 包装系数
        /// </summary>
        public int? BZXS { get; set; }

        /// <summary>
        /// 药品剂型ID；字典ID
        /// </summary>
        public string YPJXID { get; set; }
        /// <summary>
        /// 药品剂型名称；字典对应名称
        /// </summary>
        public string YPJXMC { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string YWMC { get; set; }

        /// <summary>
        /// 药品名称首拼码
        /// </summary>
        public string PY { get; set; }

        /// <summary>
        /// 药品名称五笔码
        /// </summary>
        public string WB { get; set; }

        /// <summary>
        /// 药品类型：0西药；1中成药；2中药；3其他（参照中心数据）
        /// </summary>
        public byte? YPLX { get; set; }

        /// <summary>
        /// 药性分类码：00、01、02、03、04，05
        /// </summary>
        public string YXFL { get; set; }

        /// <summary>
        /// 药性分类名：普通00、精神01、麻醉02、毒性03、放射性04，其他05
        /// </summary>
        public string YXFLM { get; set; }

        /// <summary>
        /// 剂型代码ID；字典ID
        /// </summary>
        public string JXDMID { get; set; }
        /// <summary>
        /// 剂型代码名称；字典名称
        /// </summary>
        public string JXDMMC { get; set; }
        /// <summary>
        /// 药品国别
        /// </summary>
        public string YPGB { get; set; }

        /// <summary>
        /// 生产厂家ID；JCSJ_SCCS_BZXX.ID
        /// </summary>
        public string CJID { get; set; }
        /// <summary>
        /// 生产厂家名称
        /// </summary>
        public string CJMC { get; set; }
        /// <summary>
        /// 药品本位码
        /// </summary>
        public string BWM { get; set; }

        /// <summary>
        /// 商品码
        /// </summary>
        public string SPM { get; set; }

        /// <summary>
        /// 批准文号
        /// </summary>
        public string PZWH { get; set; }

        /// <summary>
        /// 批准日期,YYYY-MM-DD
        /// </summary>
        public DateTime? PZRQ { get; set; }

        /// <summary>
        ///停用标志 0：在用，1停用
        /// </summary>
        public bool TYBZ { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreaterID { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人ID
        /// </summary>
        public string UpdaterID { get; set; }

        /// <summary>
        /// 创建人Code
        /// </summary>
        public string CreaterCode { get; set; }
        /// <summary>
        /// 修改人Code
        /// </summary>
        public string UpdaterCode { get; set; }
        /// <summary>
        /// 电子监管码
        /// </summary>
        public string DZJGM { get; set; }
        /// <summary>
        /// 上传dep标志(0：未上传，1上传成功；2;上传失败)
        /// </summary>
        public byte UpDepStatus { get; set; }

        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UpTime { get; set; }
        /// <summary>
        /// 上传返回信息
        /// </summary>
        public string UpMsg { get; set; }
        /// <summary>
        /// 数据来源（0-Excel导入；1-中心下载）
        /// </summary>
        public byte? SJLY { get; set; }
        /// <summary>
        /// 采购类别【中标药品(0)、廉价药品(1)、紧张药品(2)、低价药品(3)、备案药品(4)】
        /// </summary>
        public short? CGLB { get; set; }
        /// <summary>
        /// 管理标识（1-线上，2-线下）
        /// </summary>
        public byte? GLBS { get; set; }

        /// <summary>
        /// 医保代码
        /// </summary>
        public string YBDM { get; set; }
    }


    /// <summary>
    /// 基础数据-药品-自定义信息
    /// </summary>
    public class JCSJ_YP_ZDYXXModel : DrugBasicInfoModel, IModel
    {
        /// <summary>
        /// 审核状态；1.暂存 | 待提交；2.已提交 | 待审核；3.审核通过；4：不通过
        /// </summary>
        public byte SHZT { get; set; }

        /// <summary>
        /// 提交审核时间
        /// </summary>
        public DateTime? TJSJ { get; set; }

        /// <summary>
        /// 审阅时间
        /// </summary>
        public DateTime? SYSJ { get; set; }

        /// <summary>
        ///审核结束时间
        /// </summary>
        public DateTime? SHJSSJ { get; set; }

        /// <summary>
        /// 审核反馈结果
        /// </summary>
        public string SHFK { get; set; }
        /// <summary>
        /// 上传dep标志(0：未上传，1上传成功；2;上传失败)
        /// </summary>
        public byte UpDepStatus { get; set; }

        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UpTime { get; set; }
        /// <summary>
        /// 上传返回信息
        /// </summary>
        public string UpMsg { get; set; }
        /// <summary>
        /// 是否需要上传dep；0：不需要上传（一般是只从dep下载（或者已经上传过的）  且未修改过）；1：需要上传dep（从excel导入，数据由修改）
        /// </summary>
        public byte NeedUpToDep { get; set; }
        /// <summary>
        /// 数据来源（0-Excel导入；1-中心下载）
        /// </summary>
        public byte? SJLY { get; set; }
        /// <summary>
        /// 数据来源值（0-Excel导入；1-中心下载）
        /// </summary>
        public string SjlyValue { get; set; }
        /// <summary>
        /// 采购类别【中标药品(0)、廉价药品(1)、紧张药品(2)、低价药品(3)、备案药品(4)】
        /// </summary>
        public short? CGLB { get; set; }
        /// <summary>
        /// His编码
        /// </summary>
        public string HISCode { get; set; }
        /// <summary>
        /// 进零加成比 
        /// </summary>
        public decimal? JLJCB { get; set; }

        /// <summary>
        /// 管理标识（1-线上，2-线下）
        /// </summary>
        public byte? GLBS { get; set; }

        /// <summary>
        /// 管理标识(1-线上，2-线下）
        /// </summary>
        public string GLBSValue { get; set; }

        /// <summary>
        /// 规格图片附件
        /// </summary>
        public List<JCSJ_YP_GGFJModel> FileList { get; set; }
        /// <summary>
        /// 基药类型字典
        /// </summary>
        public List<BaseDrugTypeModel> JylxList { get; set; }

        /// <summary>
        /// 根客户ID 
        /// </summary>
        public string Curr_CustomerID { get; set; }
    }
    /// <summary>
    ///基础数据-药品-规格图片
    /// </summary>
    public class JCSJ_YP_GGFJModel : IModel
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 基础数据-药品-自定义信息ID；JCSJ_YP_ZDYXX.ID
        /// </summary>
        public string YPZDYXXID { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string WJMC { get; set; }

        /// <summary>
        /// 文件类型
        /// </summary>
        public string WJLX { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string WJLJ { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建者ID
        /// </summary>
        public string CreaterID { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人ID
        /// </summary>
        public string UpdaterID { get; set; }

        /// <summary>
        /// 顺序号
        /// </summary>
        public int SEQ { get; set; }
        /// <summary>
        /// 创建人Code
        /// </summary>
        public string CreaterCode { get; set; }
        /// <summary>
        /// 修改人Code
        /// </summary>
        public string UpdaterCode { get; set; }
        /// <summary>
        /// 药品自定义模型
        /// </summary>
        public virtual JCSJ_YP_ZDYXXModel YpZdyxxModel { get; set; }
    }
    public class BaseDrugTypeModel
    {
        /// <summary>
        /// 对应字典项名称
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 对应字典项Code
        /// </summary>
        public string Value { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class DrugBasicInfoModel
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// 药品信息 ID JCSJ_YP_BZXX.ID
        /// </summary>
        public string YPBZXXID { get; set; }
        /// <summary>
        /// 药品基础信息表
        /// </summary>
        public virtual JCSJ_YP_BZXXModel BaseData { get; set; }
        /// <summary>
        /// 基药类型；0：非基药；1：基药，2：省增补
        /// </summary>
        public string JYLX { get; set; }
        /// <summary>
        /// 基药类型值
        /// </summary>
        public string JYLXValue { get; set; }

        /// <summary>
        /// 是否GPO；0：否；1：是
        /// </summary>
        public bool? GPO { get; set; }
        /// <summary>
        /// 是否托管；0：否；1：是
        /// </summary>
        public bool? TG { get; set; }
        /// <summary>
        /// 是否定量采购；0：否；1：是
        /// </summary>
        public bool? DLCG { get; set; }
        /// <summary>
        /// 医保代码
        /// </summary>
        public string YBDM { get; set; }

        /// <summary>
        /// 阳光码
        /// </summary>
        public string YGM { get; set; }

        /// <summary>
        /// 库存上限
        /// </summary>
        public int? KCSX { get; set; }

        /// <summary>
        /// 库存下限
        /// </summary>
        public int? KCXX { get; set; }

        /// <summary>
        /// 采购药库ID
        /// </summary>
        public string CGYKID { get; set; }

        /// <summary>
        /// 采购药库名称
        /// </summary>
        public string CGYKMC { get; set; }

        /// <summary>
        /// 箱规数量
        /// </summary>
        public int? XGSL { get; set; }

        /// <summary>
        /// 目录类型；0：目录内；1：目录外
        /// </summary>
        public byte? MLLX { get; set; }

        /// <summary>
        /// 使用单位Code;记录是哪个医疗机构使用的
        /// </summary>
        public string SYDWCode { get; set; }
        /// <summary>
        ///默认供应商id
        /// </summary>
        public string MRGYSID { get; set; }
        /// <summary>
        /// 默认供应商名称
        /// </summary>
        public string MRGYSMC { get; set; }
        /// <summary>
        /// 第三方编码
        /// </summary>
        public string ID3 { get; set; }

        /// <summary>
        /// 中标价
        /// </summary>
        public decimal? ZBJ { get; set; }
        /// <summary>
        /// 进价
        /// </summary>
        public decimal? JJ { get; set; }
        /// <summary>
        /// 零售价
        /// </summary>
        public decimal? LSJ { get; set; }
        /// <summary>
        /// 是否重点监控；0：否 false；1：是 true
        /// </summary>
        public bool? ZDJK { get; set; }
        /// <summary>
        /// 门诊单位ID；字典（盒、支...）
        /// </summary>
        public string MZDWID { get; set; }
        /// <summary>
        /// 门诊单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string MZDWMC { get; set; }
        /// <summary>
        /// 门诊系数
        /// </summary>
        public decimal? MZXS { get; set; }

        /// <summary>
        /// 住院单位ID；字典（盒、支...）
        /// </summary>
        public string ZYDWID { get; set; }
        /// <summary>
        /// 住院单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string ZYDWMC { get; set; }
        /// <summary>
        /// 住院系数
        /// </summary>
        public decimal? ZYXS { get; set; }

        /// <summary>
        /// 药库单位ID；字典（盒、支...）
        /// </summary>
        public string YKDWID { get; set; }
        /// <summary>
        /// 药库单位名称；字典对应名称（盒、支...）
        /// </summary>
        public string YKDWMC { get; set; }
        /// <summary>
        /// 药库系数
        /// </summary>
        public decimal? YKXS { get; set; }
        /// <summary>
        /// 启用标志；1：已启用，0：已停用
        /// </summary>
        public bool QYBZ { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreaterID { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人ID
        /// </summary>
        public string UpdaterID { get; set; }

        /// <summary>
        /// 创建人Code
        /// </summary>
        public string CreaterCode { get; set; }
        /// <summary>
        /// 修改人Code
        /// </summary>
        public string UpdaterCode { get; set; }
        //public decimal? YJ { get; set; }
        //public decimal? PTJ { get; set; }
        //public bool? SF47 { get; set; }
    }

    /// <summary>
    /// 基础数据-药品-标准信息
    /// </summary>
    public class JCSJ_YP_BZXXModel : IModel
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 中心数据-药品信息ID
        /// </summary>
        public string ZXYPID { get; set; }

        /// <summary>
        /// 药品名称
        /// </summary>
        public string YPMC { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string SPMC { get; set; }

        /// <summary>
        /// 药品规格
        /// </summary>
        public string YPGG { get; set; }
        /// <summary>
        /// 包装规格 内容是：[药品规格]*[包装系数][最小单位]/[包装单位]
        /// </summary>
        public string BZGG { get; set; }
        /// <summary>
        /// 包装单位；字典ID
        /// </summary>
        public string BZDWID { get; set; }
        /// <summary>
        /// 包装单位名称；字典对应名称
        /// </summary>
        public string BZDWMC { get; set; }

        /// <summary>
        /// 最小单位ID；字典ID
        /// </summary>
        public string ZXDWID { get; set; }
        /// <summary>
        /// 最小单位名称；字典对应名称
        /// </summary>
        public string ZXDWMC { get; set; }
        /// <summary>
        /// 包装系数
        /// </summary>
        public int? BZXS { get; set; }

        /// <summary>
        /// 药品剂型ID；字典ID
        /// </summary>
        public string YPJXID { get; set; }
        /// <summary>
        /// 药品剂型名称；字典对应名称
        /// </summary>
        public string YPJXMC { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string YWMC { get; set; }

        /// <summary>
        /// 药品名称首拼码
        /// </summary>
        public string PY { get; set; }

        /// <summary>
        /// 药品名称五笔码
        /// </summary>
        public string WB { get; set; }

        /// <summary>
        /// 药品类型：0西药；1中成药；2中药；3其他（参照中心数据）
        /// </summary>
        public byte? YPLX { get; set; }

        #region 前端展示字段
        public string YPLXValue { get; set; }
        #endregion

        /// <summary>
        /// 药性分类码：普通00、精神01、麻醉02、毒性03、放射性04，其他05
        /// </summary>
        public string YXFL { get; set; }

        /// <summary>
        /// 药性分类名：普通00、精神01、麻醉02、毒性03、放射性04，其他05
        /// </summary>
        public string YXFLM { get; set; }

        /// <summary>
        /// 剂型代码ID；字典ID
        /// </summary>
        public string JXDMID { get; set; }
        /// <summary>
        /// 剂型代码名称；字典名称
        /// </summary>
        public string JXDMMC { get; set; }
        /// <summary>
        /// 剂型代码，前端展示字段
        /// </summary>
        public string JXDM { get; set; }
        /// <summary>
        /// 药品国别
        /// </summary>
        public string YPGB { get; set; }

        /// <summary>
        /// 生产厂家ID；JCSJ_SCCS_BZXX.ID
        /// </summary>
        public string CJID { get; set; }
        /// <summary>
        /// 生产厂家名称
        /// </summary>
        public string CJMC { get; set; }
        /// <summary>
        /// 药品本位码
        /// </summary>
        public string BWM { get; set; }

        /// <summary>
        /// 商品码
        /// </summary>
        public string SPM { get; set; }

        /// <summary>
        /// 批准文号
        /// </summary>
        public string PZWH { get; set; }

        /// <summary>
        /// 批准日期,YYYY-MM-DD
        /// </summary>
        public DateTime? PZRQ { get; set; }

        /// <summary>
        ///停用标志 0：在用，1停用
        /// </summary>
        public bool TYBZ { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 创建人ID
        /// </summary>
        public string CreaterID { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 修改人ID
        /// </summary>
        public string UpdaterID { get; set; }
        /// <summary>
        /// 创建人Code
        /// </summary>
        public string CreaterCode { get; set; }
        /// <summary>
        /// 修改人Code
        /// </summary>
        public string UpdaterCode { get; set; }
        /// <summary>
        /// 电子监管码
        /// </summary>
        public string DZJGM { get; set; }
        /// <summary>
        /// 上传dep标志(0：未上传，1上传成功；2;上传失败)
        /// </summary>
        public byte UpDepStatus { get; set; }

        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? UpTime { get; set; }
        /// <summary>
        /// 上传返回信息
        /// </summary>
        public string UpMsg { get; set; }
        /// <summary>
        /// 数据来源（0-Excel导入；1-中心下载）
        /// </summary>
        public byte? SJLY { get; set; }
        /// <summary>
        /// 采购类别【中标药品(0)、廉价药品(1)、紧张药品(2)、低价药品(3)、备案药品(4)】
        /// </summary>
        public short? CGLB { get; set; }

        /// <summary>
        /// 管理标识（1-线上，2-线下）
        /// </summary>
        public byte? GLBS { get; set; }
        /// <summary>
        /// 医保代码
        /// </summary>
        public string YBDM { get; set; }
    }
}