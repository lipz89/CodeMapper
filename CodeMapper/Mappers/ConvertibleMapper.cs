﻿using CodeMapper.Commons;
using CodeMapper.Metas;
using System;

namespace CodeMapper.Mappers
{
    internal class ConvertibleMapper : BaseMapper
    {
        private readonly TypePair pair;
        private Func<object, object> innerMapper;
        public ConvertibleMapper(TypePair pair)
        {
            innerMapper = Converter.Get(pair);
            this.pair = pair;
        }
        protected override object MapCore(object source, object target, int depth)
        {
            try
            {
                return innerMapper(source);
            }
            catch(Exception ex)
            {
                var msg = $"ConvertibleMapper转换({pair})失败 --> {ex.Message}";
                throw new Exception(msg, ex);
            }
        }
    }
}
